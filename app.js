/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

//<debug>
Ext.Loader.setPath({
    'Ext'    : 'touch/src',
    'Locale' : 'app',
    'Ux'     : './Ux'
});
//</debug>

Ext.application({
    name: 'MyDream',

    requires: [
        'Ext.MessageBox','Ext.data.JsonP','Ext.plugin.PullRefresh','Ext.data.Store','Ext.plugin.ListPaging',
        'Ux.locale.Manager',
        'Ux.locale.override.st.Component',
        'Ux.locale.override.st.Button',
        'Ux.locale.override.st.Container',
        'Ux.locale.override.st.TitleBar',
        'Ux.locale.override.st.field.Field',
        'Ux.locale.override.st.field.DatePicker',
        'Ux.locale.override.st.form.FieldSet',
        'Ux.locale.override.st.picker.Picker',
        'Ux.locale.override.st.picker.Date'
    ],

    models: [
        'Notification','Dream','CheckDream'
    ],

    stores: [
        'Notifications','Dreams'
    ],

    views: [
        'Login','Register','Remember','RegisterFB','TabsView','Notifications','MyDreams','DreamDetail','CreateDream','EditDream','DreamStep','DreamStepEdit','CheckedDreams','CheckDream','SelectDream','CheckDetail'
    ],

    controllers:[
        'Main'
    ],

    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    isDevice: true,
    isPhonegap: true,
    isDistribution: true,
    fbAppID: '523453141035869',
    fbTestUserID: '1131824789',
    push_pw_appidDebug: '148A2-0A152',
    push_pw_appid: '46BF3-62ABD',
    push_pw_appnameDebug: 'My Dreams',
    push_pw_appname: 'My Dreams Distribution',
    push_google_project_id: '910815127285',
    fbTestUserToken: 'CAAHcEZB6HK10BAJhBtaZCu4xl4f8rFlr2XzWZCAkk9m6CXsRHGUHgukgaR2oOrztuZCkMoKiugh0Hs5iXWx3nV7W9ZCaOwe9KbyThf2zooitcs83fKW50Q1lp93CezgfcWHyKWMwrZBoj9JLPZA7TKIdmarlkT57j7SncEfRFA6BNpMGtvYhrXJZCoZA1frQ3wp7Iu8vRnIq8kAZDZD',
    BASE_URL: 'http://www.mydreams.com/',

    launch: function() {
        var language = navigator.language? navigator.language.split('-')[0] : navigator.userLanguage.split('-')[0];
        if(language!='es' || language!='en'){
            language = 'es';
        }

        Ux.locale.Manager.setConfig({
            ajaxConfig : {
                method : 'GET'
            },
            language   : language,
            tpl        : './resources/locales/{locale}.json',
            type       : 'ajax'
        });

        Ux.locale.Manager.init();

        if(!this.isDistribution){
            push_pw_appname = push_pw_appnameDebug;
            push_pw_appid = push_pw_appidDebug;
        }

        if(language=='es'){
            var MB = Ext.MessageBox;
            Ext.apply(MB, {
                    YES: { text: 'Si', itemId: 'yes', ui: 'action' }
            });
            Ext.apply(MB, {
                    YESNO: [Ext.MessageBox.NO, Ext.MessageBox.YES]
            });
        }
        
        if(this.isDevice){
            //register facebook SDK
            try{
                FB.init({ appId: this.fbAppID, nativeInterface: CDV.FB, useCachedDialogs: false });    
            }catch(err){
                this.isDevice=false;
            }
            
            //register device notification
            this.bindEvents();
        }

        Ext.defer(function() { 
            Ext.fly('appLoadingIndicator').destroy();
            Ext.Viewport.add(Ext.create('MyDream.view.Login'));
        }, 200);
    },
    onUpdated: function() {
        Ext.Msg.confirm(
            "Actualización",
            "La aplicación se aplicó correctamente, deseas reiniciar?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    },
    bindEvents: function() {
        this.onDeviceReady();
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        initPushwoosh();
        //app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
});

function registerPushwooshIOS() {
         var pushNotification = window.plugins.pushNotification;

         //push notifications handler
        document.addEventListener('push-notification', function(event) {
                                var notification = event.notification;
                                console.log(notification.aps.alert);
                                
                                //to view full push payload
                                console.log(JSON.stringify(notification));
                                
                                //reset badges on icon
                                pushNotification.setApplicationIconBadgeNumber(0);
                          });

        pushNotification.registerDevice({alert:true, badge:true, sound:true, pw_appid: MyDream.app.push_pw_appid, appname:MyDream.app.push_pw_appname},
                                                                        function(status) {
                                                                                var deviceToken = status['deviceToken'];
                                                                                localStorage.setItem("pushToken",deviceToken);
                                                                                console.log('registerDevice: ' + deviceToken);
                                                                                onPushwooshiOSInitialized(deviceToken);
                                                                        },
                                                                        function(status) {
                                                                                console.warn('failed to register : ' + JSON.stringify(status));
                                                                                console.log(JSON.stringify(['failed to register ', status]));
                                                                        });
        
        //reset badges on start
        pushNotification.setApplicationIconBadgeNumber(0);
}

function onPushwooshiOSInitialized(pushToken)
{
    var pushNotification = window.plugins.pushNotification;
    //retrieve the tags for the device
    pushNotification.getTags(function(tags) {
                                                            console.warn('tags for the device: ' + JSON.stringify(tags));
                                                     },
                                                     function(error) {
                                                            console.warn('get tags error: ' + JSON.stringify(error));
                                                     });
     
    //start geo tracking. PWTrackSignificantLocationChanges - Uses GPS in foreground, Cell Triangulation in background. 
    pushNotification.startLocationTracking('PWTrackSignificantLocationChanges',
                                                                    function() {
                                                                               console.warn('Location Tracking Started');
                                                                    });
}

function registerPushwooshAndroid() {
    try{
        var pushNotification = window.plugins.pushNotification;

        //push notifications handler
        document.addEventListener('push-notification', function(event) {
                    var title = event.notification.title;
                    var userData = event.notification.userdata;

                    //dump custom data to the console if it exists
                    if(typeof(userData) != "undefined") {
                                        console.warn('user data: ' + JSON.stringify(userData));
                                }

                                //and show alert
                                console.log(title);

                                //stopping geopushes
                                pushNotification.stopGeoPushes();
                          });

        //projectid: "GOOGLE_PROJECT_ID", appid : "PUSHWOOSH_APP_ID"
        pushNotification.registerDevice({ projectid: MyDream.app.push_google_project_id, appid : MyDream.app.push_pw_appid },
                                                                        function(token) {
                                                                            console.log(token);
                                                                            localStorage.setItem("pushToken",token);
                                                                            //callback when pushwoosh is ready
                                                                            onPushwooshAndroidInitialized(token);
                                                                        },
                                                                        function(status) {
                                                                            console.log("failed to register: " +  status);
                                                                            console.log(JSON.stringify(['failed to register ', status]));
                                                                        });
    }catch(err){
        console.log(err);
    }
        
 }

function onPushwooshAndroidInitialized(pushToken)
{
        //output the token to the console
        console.warn('push token: ' + pushToken);

        var pushNotification = window.plugins.pushNotification;
        
        pushNotification.getTags(function(tags) {
                                                        console.warn('tags for the device: ' + JSON.stringify(tags));
                                                 },
                                                 function(error) {
                                                        console.warn('get tags error: ' + JSON.stringify(error));
                                                 });
         

        //set multi notificaiton mode
        //pushNotification.setMultiNotificationMode();
        //pushNotification.setEnableLED(true);
        
        //set single notification mode
        //pushNotification.setSingleNotificationMode();
        
        //disable sound and vibration
        //pushNotification.setSoundType(1);
        //pushNotification.setVibrateType(1);
        
        pushNotification.setLightScreenOnNotification(false);
        
        //goal with count
        //pushNotification.sendGoalAchieved({goal:'purchase', count:3});
        
        //goal with no count
        //pushNotification.sendGoalAchieved({goal:'registration'});

        //setting list tags
        //pushNotification.setTags({"MyTag":["hello", "world"]});
        
        //settings tags
        pushNotification.setTags({deviceName:"hello", deviceId:10},
                                                                        function(status) {
                                                                                console.warn('setTags success');
                                                                        },
                                                                        function(status) {
                                                                                console.warn('setTags failed');
                                                                        });
                
        function geolocationSuccess(position) {
                pushNotification.sendLocation({lat:position.coords.latitude, lon:position.coords.longitude},
                                                                 function(status) {
                                                                          console.warn('sendLocation success');
                                                                 },
                                                                 function(status) {
                                                                          console.warn('sendLocation failed');
                                                                 });
        };
                
        // onError Callback receives a PositionError object
        //
        function geolocationError(error) {
                console.log('code: '    + error.code    + '\n' +
                          'message: ' + error.message + '\n');
        }
        
        function getCurrentPosition() {
                navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError);
        }
        
        //greedy method to get user position every 3 second. works well for demo.
       setInterval(getCurrentPosition, 3000);
                
        //this method just gives the position once
        navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError);
                
        //this method should track the user position as per Phonegap docs.
        navigator.geolocation.watchPosition(geolocationSuccess, geolocationError, { maximumAge: 3000, enableHighAccuracy: true });

        //Pushwoosh Android specific method that cares for the battery
        pushNotification.startGeoPushes();
}

 function initPushwoosh() {

    try{
        var pushNotification = window.plugins.pushNotification;
        if(device.platform == "Android")
        {
            registerPushwooshAndroid();
            pushNotification.onDeviceReady();
        }

        if(device.platform == "iPhone" || device.platform == "iOS")
        {
            registerPushwooshIOS();
            pushNotification.onDeviceReady();
        }
    }catch(err){
        console.log(err);
    }
}

