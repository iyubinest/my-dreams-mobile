Ext.define("MyDream.view.DreamDetail", {
    extend: "Ext.Container",
    xtype: 'dreamdetail',
    alias: "widget.dreamdetail",
    requires: ['Ext.form.FieldSet', 'Ext.form.Password', 'Ext.Label', 'Ext.Img', 'Ext.util.DelayedTask','Ext.Carousel'],

    initialize: function () {
        this.callParent(arguments);
    },
    onBackTap: function () {
        this.fireEvent("backCommand", this);
    },
    onPictureButton: function(){
        this.fireEvent("takePictureCommand", this);
    },
    onEditButton: function () {
        this.fireEvent("editDreamCommand", this.response);
    },
    linkify: function(response,id){
        if(response.substring(0,10)=='data:image'){
            return response;
        }
        if(response==='' || response==undefined){
            return MyDream.app.BASE_URL+'dream-pic?id='+id;
        }
        return MyDream.app.BASE_URL+response;
    },
    loadDataView: function(response){

        var me = this;
        me.response = response;

        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: Ux.locale.Manager.get('please_wait')
        });

        var panel = this;
        var id = response.get('idDream');
        me.dreamId = id;
        var added = false;

        var backButton = {
            xtype: "button",
            text: '',
            baseCls: 'mBackBtn',
            pressedCls: 'mBackBtnPressed',
            ui: 'back',
            itemId: 'backButton',
            handler: this.onBackTap,
            scope: this
        };

        var topToolbar = {
            xtype: 'toolbar',
            docked: "top",
            baseCls: 'header',
            title: response.get('name'),
            items:[
                backButton
            ]
        };

        me.completion = response.get('completion');

        var percentPanel = {
            xtype: 'panel',
            itemId: 'percentPanel',
            html: '<div class="dream-percent"><h2><span id="percent_number">'+me.completion+'%</span>'+Ux.locale.Manager.get('dramdetail.completed')+'</h2></div>'
        };

        var day = response.get('deadline').substring(0,2);
        var month = response.get('deadline').substring(3,5);
        var year = response.get('deadline').substring(6,10);
        var fDate = new Date(year,month,day,0,0,0,0);

        me.tipo = response.get('type');
        var dataPanel = {
            xtype: 'panel',
            itemId: 'dataPanel',
            html: '<div class="dream-info clearfix"><h2></h2><div class="step-date"> <span>'+day+'</span><span>'+month+'</span><span>'+year+'</span></div></div>'
        };

        Ext.Ajax.request({
            url: MyDream.app.BASE_URL+'categories?lang='+Ux.locale.Manager.getLanguage(),
            timeout: 40000,
            success: function (response) {
                var res = Ext.JSON.decode(response.responseText);
                var dataPanel = me.down('#dataPanel');
                var nameType = '';
                for(var i=0 ; i<res.length ; i++){
                    if(res[i].id==me.tipo){
                        nameType = res[i].description;
                    }
                    if(i==res.length-1 && nameType==''){
                        nameType='N/A';
                    }
                }
                dataPanel.setHtml('<div class="dream-info clearfix"><h2>'+nameType+'</h2><div class="step-date"> <span>'+day+'</span><span>'+month+'</span><span>'+year+'</span></div></div>');
            },
            failure: function (response) {
                var dataPanel = me.down('#dataPanel');
                dataPanel.setHtml('<div class="dream-info clearfix"><h2>No Disponible</h2><div class="step-date"> <span>'+day+'</span><span>'+month+'</span><span>'+year+'</span></div></div>');
            } 
        });

        var stepItems = [];
        var columnCounter = 0;
        var dreamsteps = response.get('steps');
        me.totalStepWeights = 0;
        me.totalStepFinished = 0;
        for(var i = 0 ; i<dreamsteps.length ; i++){
            me.totalStepWeights += dreamsteps[i].w;
            if(dreamsteps[i].completed==1)
                me.totalStepFinished++;
        }
        me.totalStepWeights++;

        var lastStepListener = {
            tap: {
                element: 'element',
                delegate: 'div.last-step-grid',
                fn: function(me_e, e, eOpts) {
                    var element = Ext.get(me_e.delegatedTarget);
                    var id = parseInt(element.id);
                    if(me.completion<100)
                    if(dreamsteps.length==0 || me.totalStepFinished==dreamsteps.length){
                        Ext.Msg.confirm(
                            Ux.locale.Manager.get('dramdetail.msg_title_complete'),
                            Ux.locale.Manager.get('dramdetail.msg_complete'),
                            function(buttonId) {
                                if (buttonId === 'yes') {
                                    Ext.Viewport.setMasked({
                                        xtype: 'loadmask',
                                        message: Ux.locale.Manager.get('please_wait')
                                    });
                                    try{
                                        Ext.Ajax.request({
                                            url: MyDream.app.BASE_URL+'completedream?dream='+me.dreamId,
                                            timeout: 160000,
                                            success: function (response) {
                                                var loginResponse = Ext.JSON.decode(response.responseText);
                                                var loginValidation = loginResponse.status + "";
                                                if (loginValidation === "true") {
                                                    var lastStepPanel = document.getElementById('last-step');
                                                    lastStepPanel.innerHTML = '<p>'+Ux.locale.Manager.get('dramdetail.dream_completed')+'</p>';

                                                    if(me.totalStepFinished==dreamsteps.length){
                                                        var elements = document.getElementsByClassName("last-step");
                                                        for(var i=0; i<elements.length; i++) {
                                                            elements[i].innerHTML = '<p>'+Ux.locale.Manager.get('dramdetail.dream_completed')+'</p>';
                                                        }
                                                    }

                                                    if(me.totalStepFinished==dreamsteps.length){
                                                        var elements = document.getElementsByClassName("dreamdone");
                                                        for(var i=0; i<elements.length; i++) {
                                                            elements[i].style.visibility='hidden';
                                                        }
                                                    }
                                                    
                                                    me.completion = 100;
                                                    var percentPanel = me.down('#percentPanel');
                                                    percentPanel.setHtml('<div class="dream-percent"><h2><span id="percent_number">'+me.completion+'%</span>'+Ux.locale.Manager.get('dramdetail.completed')+'</h2></div>');
                                                    me.updateList = true;
                                                }
                                                Ext.Viewport.setMasked(false);
                                            },
                                            failure: function (response) {
                                                Ext.Viewport.setMasked(false);
                                            } 
                                        });
                                    }catch(err){
                                        Ext.Viewport.setMasked(false);
                                    }
                                }
                            }
                        );
                    }
                }
            }
        };

        for(var i = 0 ; i<dreamsteps.length ; i++){
            var day = dreamsteps[i].deadline.substring(0,2);
            var month = dreamsteps[i].deadline.substring(3,5);
            var year = dreamsteps[i].deadline.substring(6,10);

            var classDone='';
            var listener = null;
            if(dreamsteps[i].completed==1){
                classDone = 'stepdone';
            }else{
                listener = {
                    tap: {
                        element: 'element',
                        delegate: 'div .steps-grid',
                        fn: function(me_e, e, eOpts) {
                            var element = Ext.get(me_e.delegatedTarget);
                            var id = parseInt(element.id);
                            if( (dreamsteps[id].completed==0 && id==0) || ( dreamsteps[id].completed==0 && id>=1 && dreamsteps[id-1].completed==1 ) ){
                                Ext.Msg.confirm(
                                    Ux.locale.Manager.get('dramdetail.msg_title'),
                                    Ux.locale.Manager.get('dramdetail.msg'),
                                    function(buttonId) {
                                        if (buttonId === 'yes') {
                                            Ext.Viewport.setMasked({
                                                xtype: 'loadmask',
                                                message: Ux.locale.Manager.get('please_wait')
                                            });
                                            try{
                                                Ext.Ajax.request({
                                                    url: MyDream.app.BASE_URL+'completestep?step='+dreamsteps[id].idStep+'&dream='+me.dreamId+'&value=true',
                                                    timeout: 10000,
                                                    success: function (response) {
                                                        var loginResponse = Ext.JSON.decode(response.responseText);
                                                        var loginValidation = loginResponse.status + "";
                                                        if (loginValidation === "true") {
                                                            element.dom.children[0].className += ' stepdone';
                                                            dreamsteps[id].completed=1;
                                                            me.completion += Math.round(dreamsteps[id].w*100/me.totalStepWeights);
                                                            if(me.completion>=100){
                                                                me.completion=100;
                                                            }
                                                            me.totalStepFinished++;
                                                            if(me.totalStepFinished==dreamsteps.length){
                                                                var elements = document.getElementsByClassName("dreamdone");
                                                                for(var i=0; i<elements.length; i++) {
                                                                    elements[i].style.visibility='visible';
                                                                }
                                                            }
                                                            var percentPanel = me.down('#percentPanel');
                                                            percentPanel.setHtml('<div class="dream-percent"><h2><span id="percent_number">'+me.completion+'%</span>'+Ux.locale.Manager.get('dramdetail.completed')+'</h2></div>');
                                                        }
                                                        Ext.Viewport.setMasked(false);
                                                    },
                                                    failure: function (response) {
                                                        Ext.Viewport.setMasked(false);
                                                    } 
                                                });
                                            }catch(err){
                                                Ext.Viewport.setMasked(false);
                                            }
                                        }
                                    }
                                );
                            }
                        }
                    }
                };
            }
                
            var stepPanel = {
                xtype: 'panel',
                itemId: 'stepsPanel',
                html: '<div id="'+i+'" class="steps-grid"><div class="dream-step '+classDone+'" ><p id="'+i+'" >'+dreamsteps[i].name+'</p><span id="'+i+'" class="step-number">'+(i+1)+'</span><div id="'+i+'" class="step-date"> <span id="'+i+'" >'+day+'</span><span id="'+i+'" >'+month+'</span><span id="'+i+'" >'+year+'</span> </div></div></div>',
                listeners: listener
            };
            
            if(dreamsteps[i].name != undefined)
                stepItems.push(stepPanel);

            if((i+1)%3==0){
                columnCounter++;
            }
            if(i==dreamsteps.length-1){
                var sDate = new Date();
                var millisecondsPerDay = 1000 * 60 * 60 * 24;
                var millisBetween = fDate.getTime() - sDate.getTime();
                var enddays = millisBetween / millisecondsPerDay;

                // Round down.
                enddays = Math.floor(enddays);
                if(enddays==undefined || enddays<0){
                    enddays=0;
                }

                if(me.completion==100 || me.completion=='100'){
                    enddays = '<p>'+Ux.locale.Manager.get('dramdetail.dream_completed')+'</p>';   
                }else{
                    enddays = '<p>'+Ux.locale.Manager.get('dramdetail.end_days_before')+' <span>'+enddays+' '+Ux.locale.Manager.get('dramdetail.end_days_after')+'</span> '+Ux.locale.Manager.get('dramdetail.end_days_complement')+'</p>'
                }

                //var width = 3- ( (i+1) - ( parseInt(columnCounter) * 3 ) ) ;
                var width = 1;

                var lastStepPanel = {
                    xtype: 'panel',
                    itemId: 'stepsPanel',
                    cls: 'lastStepPanel',
                    html: '<div class="last-step-grid" style="width:'+width+'00% !important;"><div id="last-step" class="last-step">'+enddays+'</div><div id="dreamdone" class="dreamdone">'+Ux.locale.Manager.get('dramdetail.end_dream')+'</div></div>',
                    listeners: lastStepListener
                };
                stepItems.push(lastStepPanel);
            }
        }

        if(dreamsteps.length==0){
            var sDate = new Date();
            var millisecondsPerDay = 1000 * 60 * 60 * 24;
            var millisBetween = fDate.getTime() - sDate.getTime();
            var enddays = millisBetween / millisecondsPerDay;

            // Round down.
            enddays = Math.floor(enddays);
            if(enddays==undefined || enddays<0){
                enddays=0;
            }

            if(me.completion==100 || me.completion=='100'){
                enddays = '<p>'+Ux.locale.Manager.get('dramdetail.dream_completed')+'</p>';    
            }else{
                enddays = '<p>'+Ux.locale.Manager.get('dramdetail.end_days_before')+' <span>'+enddays+' '+Ux.locale.Manager.get('dramdetail.end_days_after')+'</span> '+Ux.locale.Manager.get('dramdetail.end_days_complement')+'</p>'
            }

            var lastStepPanel = {
                xtype: 'panel',
                itemId: 'stepsPanel',
                cls: 'lastStepPanel',
                html: '<div class="last-step-grid" style="width:100% !important;"><div id="last-step" class="last-step">'+enddays+'</div><div id="dreamdone" class="dreamdone">'+Ux.locale.Manager.get('dramdetail.end_dream')+'</div></div>',
                listeners: lastStepListener
            };
            stepItems.push(lastStepPanel);
        }

        var stepsPanel = {
            xtype: 'panel',
            itemId: 'stepsPanel',
            cls: 'steps-map',
            items: stepItems
        };

        var pictureButton = {
            xtype: "button",
            text: Ux.locale.Manager.get('dramdetail.add_images'),
            baseCls: 'dream-gallery',
            pressedCls: 'mPictureButton',
            itemId: 'pictureButton',
            handler: this.onPictureButton,
            scope: this
        };

        var items = [];
        //items.push(pictureButton);
        var dreamimages = response.get('imgs');
        if(dreamimages){
            for (var i = 0; i < dreamimages.length; i++) {
                items.push({
                    html: '<img src="' + me.linkify(dreamimages[i],id) + '"/>'
                });
            };
        }else{
            var defaultImage = response.get('defaultImage');
            if(defaultImage){
                items.push({
                    html: '<img src="' + me.linkify(defaultImage,id) + '"/>'
                });
            }
        }

        var carousel = new Ext.Carousel({
            cardSwitchAnimation: 'slide',
            ui: 'light',
            baseCls: 'mCarouselItem',
            items: items,
            style: 'background: #000',
            itemId: 'carousel',
            directionLock: true
        });

        var editButton = {
                        xtype: 'button',
                        itemId: 'logInButton',
                        ui: 'action',
                        padding: '10px',
                        text: Ux.locale.Manager.get('dramdetail.edit'),
                        baseCls: 'mButton',
                        cls: 'mButtonCentered',
                        pressedCls: 'mButtonPressed',
                        handler: this.onEditButton,
                        scope: this
                    };

        if(!added){
            panel.add([
                topToolbar,
                carousel,
                percentPanel,
                dataPanel,
                stepsPanel,
                editButton
            ]);
            added=true;
            Ext.Viewport.setMasked(false);

            if(me.totalStepFinished==dreamsteps.length && me.completion<100){
                document.getElementById('dreamdone').style.visibility='visible';
            }
        }
    },
    config: {
        layout: {
            type: 'vbox'
        },
        scrollable: true
    }
});