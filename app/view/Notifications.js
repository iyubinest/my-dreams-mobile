Ext.define('MyDream.view.Notifications', {
    extend: 'Ext.Container',
	requires : ['Ext.dataview.List','MyDream.store.Notifications'],
    xtype: 'notifications',
    alias: "widget.notifictions",
    mStore: null,

    updateData: function(){
        //this.mStore.load();
        this.mStore.sync();
    },
    initialize: function(){
    	
    	this.callParent(arguments);
    	var me = this;
    	me.mStore = new Ext.create('Ext.data.Store',{
            autoLoad: false, //NOTE: set to false so that I can manually call store.load()
            clearOnPageLoad: true,
            model: 'MyDream.model.Notification',
            pageSize: 10,
            fields: ['id','subject', 'description', 'date'],
            listeners:{
                beforeload: function(store, opt){
                    //store.getProxy().clear();
                    store.data.clear();
                },
                load: function(store) {
                    Ext.Viewport.setMasked(false);
                }
            },
            proxy: {
                type: 'ajax',
                url: MyDream.app.BASE_URL+'mnotifications?lang='+Ux.locale.Manager.getLanguage(),
                reader: {
                    type: 'json',
                    rootProperty: 'comp'
                }
            }
        });

    	var list = {
			xtype: 'list',
			store: me.mStore,
            itemCls: 'mNotification',
            disableSelection: true,
			itemTpl: new Ext.XTemplate('<img class="mNotificationImage" src="{[this.linkify(values.dreamImage)]}"/><div class="mNotifictionDate">{[this.datify(values.date)]}</div><div class="mNotifictionName">{subject}</div><div class="mNotifictionDescription">{description}</div>',
            {
                linkify: function(response){
                    if(response==='' || response==undefined){
                        return MyDream.app.BASE_URL+'res/img/content/icon-notification.png';
                    }
                    return MyDream.app.BASE_URL+response;
                },
                datify: function(response){
                	var day = response.substring(0,2);
                	var month = response.substring(3,5);
                	var year = response.substring(6,10);
                	return '<span>'+day+'</span><span>'+month+'</span><span>'+year+'</span>'
                }
            }),
			plugins: [
	            {
                    xclass: 'Ext.plugin.PullRefresh',
                    pullText: Ux.locale.Manager.get('slide_to_update'),
                    releaseText: Ux.locale.Manager.get('leave_to_update'),
                    loadedText: Ux.locale.Manager.get('updated'),
                    loadingText: Ux.locale.Manager.get('updating'),
                    lastUpdatedText: Ux.locale.Manager.get('last_update'),
                    baseCls: 'pull'                   
                }
	        ]
	    };
	    me.mStore.load();
	    this.add([list])
    },
    config: {
		fullscreen: true,
		style: 'background-color: white;height:100%;width:100%;',
		layout: 'fit'
    }
});
