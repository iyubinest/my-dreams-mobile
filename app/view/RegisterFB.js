Ext.define("MyDream.view.RegisterFB", {
    extend: "Ext.Container",
    alias: "widget.registerfb",
    requires: ['Ext.form.FieldSet', 'Ext.form.Password', 'Ext.Label', 'Ext.Img', 'Ext.util.DelayedTask'],

    accessToken: null,
    username: null,

    onRegisterTap: function(){

        var me = this;
        nameTextField = me.down('#nameTextField');
        usernameTextField = me.down('#usernameTextField');
        emailTextField = me.down('#emailTextField');
        label = me.down('#signInFailedLabel');

        name = nameTextField.getValue();
        email = emailTextField.getValue()

        var valid = true;

        if(name === ''){
            valid = false;
            nameTextField.addCls('errorInput');
        }else{
            nameTextField.removeCls('errorInput');
        }
        
        if(email === ''){
            valid = false;
            emailTextField.addCls('errorInput');
        }else{
            emailTextField.removeCls('errorInput');
        }

        if (valid){
                Ext.Viewport.setMasked({
                    xtype: 'loadmask',
                    message: Ux.locale.Manager.get('please_wait')
                });
                var task = Ext.create('Ext.util.DelayedTask', function () {
                Ext.Ajax.request({
                    url: MyDream.app.BASE_URL+'mregister',
                    method: 'post',
                    timeout: 30000,
                    params: {
                        username: me.username,
                        email: email,
                        name: name,
                        password: me.accessToken
                    },
                    success: function (response){
                        try{
                            var loginResponse = Ext.JSON.decode(response.responseText);
                            var loginValidation = loginResponse.status + "";
                            Ext.Viewport.setMasked(false);
                            if (loginValidation === "true") {
                                me.fireEvent("registerCommand", this, username, password);    
                            }else{
                                errorName = loginResponse.err.email;
                                if(errorName)
                                    label.setHtml('<p>'+errorName+'</p>');
                                errorName = loginResponse.err.username;
                                if(errorName)
                                    label.setHtml('<p>'+errorName+'</p>');
                            }
                        }catch(err){
                            Ext.Viewport.setMasked(false);
                        }
                    },
                    failure: function (response) {
                        Ext.Viewport.setMasked(false);
                        Ext.Msg.alert(Ux.locale.Manager.get('register.failure'));
                    } 
                });
            });
            task.delay(500);
        }else{
            label.setHtml('<p>'+Ux.locale.Manager.get('form_validation_error')+'</p>');
        }
    },
    initialize: function () {
        this.callParent(arguments);
    },
    loadDataView: function(username, email, name,at) {

        var me = this;
        me.accessToken = at;
        me.username = username;

        var backButton = {
            xtype: "button",
            text: '',
            baseCls: 'mBackBtn',
            pressedCls: 'mBackBtnPressed',
            ui: 'back',
            itemId: 'backButton',
            handler: this.onBackTap,
            scope: this
        };


        var topToolbar = {
            xtype: 'toolbar',
            docked: "top",
            title: Ux.locale.Manager.get('register.title'),
            baseCls: 'header',
            items:[
                backButton
            ]
        };

        var labelError = {
                        xtype: 'label',
                        html: '<p>   </p>',
                        itemId: 'signInFailedLabel',
                        hidden: false,
                        baseCls: 'mLabel',
                        cls: 'error'
                    };

        var labelUser = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('register.username')
                    };

        var inputUser = {
                        xtype: 'textfield',
                        baseCls: 'mInputForm',
                        itemId: 'usernameTextField',
                        name: 'usernameTextField',
                        required: true,
                        hidden: true,
                        value: username
                    };

        var labelEmail = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('register.email')
                    };

        var inputEmail = {
                        xtype: 'textfield',
                        itemId: 'emailTextField',
                        name: 'emailTextField',
                        baseCls: 'mInputForm',
                        required: true,
                        value: email
                    };

        var labelName = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('register.name')
                    };

        var inputName = {
                        xtype: 'textfield',
                        itemId: 'nameTextField',
                        name: 'nameTextField',
                        baseCls: 'mInputForm',
                        required: true,
                        value: name
                    };

        var fieldSet = {
                        xtype: 'fieldset',
                        title: Ux.locale.Manager.get('register.fieldset'),
                        baseCls: 'mLabel',
                        items: [labelUser,inputUser,labelName,inputName,labelEmail,inputEmail]
                    };

        var registerButton = {
                        xtype: 'button',
                        itemId: 'logInButton',
                        ui: 'action',
                        padding: '10px',
                        text: Ux.locale.Manager.get('register.btn'),
                        baseCls: 'mButton',
                        pressedCls: 'mButtonPressed',
                        handler: this.onRegisterTap,
                        scope: this
                    };

        

        this.add([
            topToolbar,
            labelError,
            fieldSet,
            registerButton
        ]);
    },
    onBackTap: function () {
        this.fireEvent("backCommand", this);
    },
    config: {
        layout: {
            type: 'vbox'
        },
        scrollable: true
    }
});