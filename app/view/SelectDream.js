Ext.define('MyDream.view.SelectDream', {
    extend: 'Ext.Container',
    requires : ['Ext.dataview.List','MyDream.model.Notification'],
    xtype: 'selectdream',
    alias: "widget.selectdream",
    mStore: null,
    checkDream: null,
    loadDataView: function(check){

        var me = this;
        me.checkDream = check;
        me.mStore = new Ext.create('Ext.data.Store',{
                autoLoad: true, //NOTE: set to false so that I can manually call store.load()
                clearOnPageLoad: true,
                model: 'MyDream.model.Dream',
                fields: ['idDream','name', 'completion','completion_inverted', 'defaultImage','steps','deadline'],
                proxy: {
                    type: 'ajax',
                    url: MyDream.app.BASE_URL+'mmydreams?offset=0',
                    reader: {
                        type: 'json',
                        rootProperty: 'comp'
                    }
                }
            });

        var backButton = {
            xtype: "button",
            text: '',
            baseCls: 'mBackBtn',
            pressedCls: 'mBackBtnPressed',
            ui: 'back',
            itemId: 'backButton',
            handler: this.onBackTap,
            scope: this
        };

        var topToolbar = {
            xtype: 'toolbar',
            docked: "top",
            baseCls: 'header',
            title: Ux.locale.Manager.get('selectdream.title'),
            items:[
                backButton
            ]
        };

        var list = {
            xtype: 'list',
            itemTpl: new Ext.XTemplate('<div class="item-dream"><h3>{name}</h3><p class="percent-dream-completion"> {completion}% </p><div class="dream-color-overlay" style="height: {completion_inverted}% !important;"></div><img class="dream-cover" style="background-image:url({[this.linkify(values.defaultImage,values.idDream)]});background-size:contain;background-position:center;height:100%;width:100%;background-repeat:no-repeat;" /></div>',
            {
                linkify: function(response,id){
                    if(response.substring(0,10)=='data:image/'){
                        return response;
                    }
                    if(response==='' || response==undefined){
                        return MyDream.app.BASE_URL+'dream-pic?id='+id;
                    }
                    return MyDream.app.BASE_URL+response;
                }
            }),
            store: me.mStore,
            baseCls: 'mGridlist',
            plugins: [
                {
                    xclass: 'Ext.plugin.ListPaging', // part of Sencha Touch
                    autoPaging: false,                // page on swipe down
                    loadMoreText : Ux.locale.Manager.get('load_more'),  // text to show when more records
                    noMoreRecordsText : Ux.locale.Manager.get('load_more'),
                    pageSize: 10,
                    clearOnPageLoad: false
                },
                {
                    xclass: 'Ext.plugin.PullRefresh',
                    pullText: Ux.locale.Manager.get('slide_to_update'),
                    releaseText: Ux.locale.Manager.get('leave_to_update'),
                    loadedText: Ux.locale.Manager.get('updated'),
                    loadingText: Ux.locale.Manager.get('updating'),
                    lastUpdatedText: Ux.locale.Manager.get('last_update'),
                    baseCls: 'pull'                   
                }
            ],
            listeners:{
                itemtap: {fn: this.onItemTap, scope: this}
            }
        };
        this.add([topToolbar,list])
    },
    config: {
        fullscreen: true,
        style: 'background-color: transparent;height:100%;width:100%;',
        layout: 'fit'
    },
    onBackTap: function(){
        this.fireEvent("backCommand", this);
    },
    onItemTap: function(list, index, target, record, e, eOpts){
        var me = this;
        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: Ux.locale.Manager.get('please_wait')
        });
        me.checkDream.idDream = record.get('idDream');
        Ext.Ajax.request({
            url: MyDream.app.BASE_URL+'createcd',
            method: 'post',
            timeout: 200000,
            params: {
                dream: me.checkDream.idDream,
                name: me.checkDream.desc,
                city: me.checkDream.city,
                lat: me.checkDream.lat, 
                lng: me.checkDream.lng,
                img: me.checkDream.img
            },
            success: function (response) {
                try{
                    var loginResponse = Ext.JSON.decode(response.responseText);
                    var loginValidation = loginResponse.status + "";
                    if (loginValidation === "true") {
                        console.log(response.responseText);
                        me.fireEvent("backCommand", record,true);
                    }
                    Ext.Viewport.setMasked(false);
                }catch(err){
                    Ext.Viewport.setMasked(false);
                    Ext.Msg.alert(Ux.locale.Manager.get('selectdream.tryagain'));
                }
            },
            failure: function (response) {
                console.log(response);
                Ext.Viewport.setMasked(false);
                Ext.Msg.alert(Ux.locale.Manager.get('selectdream.tryagain'));
            } 
        });
    }
});
