Ext.define("MyDream.view.Login", {
    extend: "Ext.Container",
    xtype: 'login',
    alias: "widget.login",
    requires: ['Ext.form.FieldSet', 'Ext.form.Password', 'Ext.Label', 'Ext.Img', 'Ext.util.DelayedTask'],

    initialize: function () {

        this.callParent(arguments);

        this.fireEvent('checkSignInCommand',this);

        var logo = {
            xtype: 'image',
            baseCls: 'backHeader',
            src: './resources/img/logo-splash.png'
        }
        var topToolbar = {
            xtype: 'toolbar',
            docked: "top",
            cls: 'headerlogo',
            items:[
                logo
            ]
        };

        var labelError = {
                        xtype: 'label',
                        html: '<p>   </p>',
                        itemId: 'signInFailedLabel',
                        hidden: false,
                        baseCls: 'mLabel',
                        cls: 'error'
                    };

        var labelUser = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('login.username')
                    };

        var inputUser = {
                        xtype: 'textfield',
                        baseCls: 'mInputForm',
                        itemId: 'userNameTextField',
                        name: 'userNameTextField',
                        required: true
                    };

        var labelPass = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('login.password')
                    };

        var inputPass = {
                        xtype: 'passwordfield',
                        itemId: 'passwordTextField',
                        name: 'passwordTextField',
                        baseCls: 'mInputForm',
                        required: true
                    };

        var fieldSet = {
                        xtype: 'fieldset',
                        title: Ux.locale.Manager.get('login.fieldset'),
                        baseCls: 'mLabel',
                        items: [labelUser,inputUser,labelPass,inputPass]
                    };

        var loginButton = {
                        xtype: 'button',
                        itemId: 'logInButton',
                        ui: 'action',
                        padding: '10px',
                        text: Ux.locale.Manager.get('login.btn'),
                        baseCls: 'mButton',
                        pressedCls: 'mButtonPressed',
                        handler: this.onLoginTap,
                        flex: 1,
                        scope: this
                    };

        var registerButton = {
                        xtype: 'button',
                        itemId: 'registerButton',
                        ui: 'action',
                        padding: '10px',
                        text: Ux.locale.Manager.get('login.register'),
                        baseCls: 'mButton',
                        pressedCls: 'mButtonPressed',
                        handler: this.onRegisterTap,
                        flex: 1,
                        scope: this
                    };

        var buttonsWrapper = {
            xtype: 'panel',
            baseCls: 'margined',
            layout: {type:'hbox',align:'stretch'},
            items: [
                loginButton,registerButton
            ]
        }

        var facebookButton = {
                        xtype: 'button',
                        itemId: 'facebookButton',
                        ui: 'action',
                        padding: '10px',
                        text: Ux.locale.Manager.get('login.facebook'),
                        baseCls: 'mButtonFacebbok',
                        handler: this.onFacebookTap,
                        scope: this
                    };

        var recoveryButton = {
            xtype: 'button',
            itemId: 'recoveryButton',
            ui: 'action',
            padding: '10px',
            text: Ux.locale.Manager.get('login.recovery'),
            baseCls: 'mButton',
            pressedCls: 'mButtonPressed',
            cls: 'mButtonCentered',
            handler: this.onRecoveryTap,
            scope: this
        };

        this.add([
            topToolbar,
            labelError,
            fieldSet,
            buttonsWrapper,
            recoveryButton,
            facebookButton
        ]);
    },
    onLoginTap: function () {
        var me = this;
        var usernameField = me.down('#userNameTextField');
        var passwordField = me.down('#passwordTextField');
        var label = me.down('#signInFailedLabel');
        var username = usernameField.getValue();
        var password = passwordField.getValue();

        // Using a delayed task in order to give the hide animation above
        // time to finish before executing the next steps.
        var task = Ext.create('Ext.util.DelayedTask', function () {
            label.setHtml('<p></p>');
            me.fireEvent('signInCommand', me, username, password);
        });
        task.delay(500);
    },
    onRegisterTap: function () {
        this.fireEvent("goRegisterCommand", this);
    },
    onRecoveryTap: function () {
        this.fireEvent("recoveryCommand", this);
    },
    onFacebookTap: function () {
        this.fireEvent("facebookCommand", this);
    },
    showSignInFailedMessage: function(message){
        var label = this.down('#signInFailedLabel');
        label.setHtml(message);
        usernameField = this.down('#userNameTextField');
        passwordField = this.down('#passwordTextField');
        usernameField.addCls('errorInput');
        passwordField.addCls('errorInput');
    },
    config: {
        layout: {
            type: 'vbox'
        },
        scrollable: true
    }
});