Ext.define("MyDream.view.CheckDetail", {
    extend: "Ext.Container",
    xtype: 'checkdetail',
    alias: "widget.checkdetail",
    requires: ['Ext.form.FieldSet', 'Ext.form.Password', 'Ext.Label', 'Ext.Img', 'Ext.util.DelayedTask','Ext.Carousel'],

    tipo: 0,
    percent: 0,
    updateList: false,

    initialize: function () {
        this.callParent(arguments);
    },
    onBackTap: function () {
        this.fireEvent("backCommand", this,this.updateList);
    },
    linkify: function(response){
        if(response.substring(0,10)=='data:image'){
            return response;
        }
        return MyDream.app.BASE_URL+response;
    },
    loadDataView: function(response){

        var me = this;
        me.response = response;

        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: Ux.locale.Manager.get('please_wait')
        });

        var panel = this;
        var id = response.get('idDream');
        var dateData = response.get('date');
        var day = dateData.substring(0,2);
        var month = dateData.substring(3,5);
        var year = dateData.substring(6,10);
        var dateHtml = '<div class="mNotifictionDate"><span>'+day+'</span><span>'+month+'</span><span>'+year+'</span></div>'
        me.dreamId = id;
        var added = false;

        var backButton = {
            xtype: "button",
            text: '',
            baseCls: 'mBackBtn',
            pressedCls: 'mBackBtnPressed',
            ui: 'back',
            itemId: 'backButton',
            handler: this.onBackTap,
            scope: this
        };

        var topToolbar = {
            xtype: 'toolbar',
            docked: "top",
            baseCls: 'header',
            title: Ux.locale.Manager.get('checkdetail.title'),
            //title: response.get('name'),
            items:[
                backButton
            ]
        };

        var labelDesc = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        cls: 'checkDesc',
                        html: response.get('name')
                    };

        var labelCity = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: '<div class="mLocationCheck"><p>'+response.get('city')+'</p></div>'+dateHtml
                    };

        var checkImage = {
                        xtype: 'image',
                        baseCls: 'checkImage',
                        src: me.linkify(response.get('img'))
                    };

        var fieldSet = {
                        xtype: 'fieldset',
                        baseCls: 'mCheckDetail',
                        items: [checkImage,labelCity,labelDesc]
                    };

        if(!added){
            panel.add([
                topToolbar,
                fieldSet
            ]);
            added=true;
            Ext.Viewport.setMasked(false);
        }
    },
    config: {
        layout: {
            type: 'vbox'
        },
        baseCls: 'mCheckDetail',
        scrollable: true
    }
});