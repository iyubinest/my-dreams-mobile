Ext.define("MyDream.view.Remember", {
    extend: "Ext.Container",
    alias: "widget.remember",
    requires: ['Ext.form.FieldSet', 'Ext.form.Password', 'Ext.Label', 'Ext.Img', 'Ext.util.DelayedTask'],

    onRememberTap: function(){

        var me = this;
        emailTextField = me.down('#emailTextField');
        label = me.down('#signInFailedLabel');
        email = emailTextField.getValue();
        
        var valid = true;
        if(email === ''){
            valid = false;
            emailTextField.addCls('errorInput');
        }else{
            emailTextField.removeCls('errorInput');
        }

        if (valid){
                Ext.Viewport.setMasked({
                    xtype: 'loadmask',
                    message: Ux.locale.Manager.get('please_wait')
                });
                var task = Ext.create('Ext.util.DelayedTask', function () {
                Ext.Ajax.request({
                    url: MyDream.app.BASE_URL+'recover-password',
                    method: 'post',
                    timeout: 30000,
                    params: {
                        email: email
                    },
                    success: function (response){
                        try{
                            var loginResponse = Ext.JSON.decode(response.responseText);
                            var loginValidation = loginResponse.status + "";
                            Ext.Viewport.setMasked(false);
                            if (loginValidation === "true") {
                                Ext.Msg.alert(Ux.locale.Manager.get('remember.success'));
                                me.fireEvent("backCommand", this);
                            }else{
                                errorName = loginResponse.err.email;
                                if(errorName)
                                    label.setHtml('<p>'+errorName+'</p>');
                                errorName = loginResponse.err.username;
                                if(errorName)
                                    label.setHtml('<p>'+errorName+'</p>');
                            }
                        }catch(err){
                            Ext.Viewport.setMasked(false);
                        }
                    },
                    failure: function (response) {
                        Ext.Viewport.setMasked(false);
                        Ext.Msg.alert(Ux.locale.Manager.get('remember.failure'));
                    } 
                });
            });
            task.delay(500);
        }else{
            label.setHtml('<p>'+Ux.locale.Manager.get('form_validation_error')+'</p>');
        }
    },
    initialize: function () {

        this.callParent(arguments);

        var backButton = {
            xtype: "button",
            text: '',
            baseCls: 'mBackBtn',
            pressedCls: 'mBackBtnPressed',
            ui: 'back',
            itemId: 'backButton',
            handler: this.onBackTap,
            scope: this
        };

        var topToolbar = {
            xtype: 'toolbar',
            docked: "top",
            title: Ux.locale.Manager.get('remember.title'),
            baseCls: 'header',
            items:[
                backButton
            ]
        };

        var labelError = {
                        xtype: 'label',
                        html: '<p>   </p>',
                        itemId: 'signInFailedLabel',
                        hidden: false,
                        baseCls: 'mLabel',
                        cls: 'error'
                    };

        var labelEmail = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('remember.email')
                    };

        var inputEmail = {
                        xtype: 'textfield',
                        baseCls: 'mInputForm',
                        itemId: 'emailTextField',
                        name: 'emailTextField',
                        required: true
                    };

        var fieldSet = {
                        xtype: 'fieldset',
                        title: Ux.locale.Manager.get('remember.fieldset'),
                        baseCls: 'mLabel',
                        items: [labelEmail,inputEmail]
                    };

        var rememberButton = {
                        xtype: 'button',
                        itemId: 'rememberButton',
                        ui: 'action',
                        padding: '10px',
                        text: Ux.locale.Manager.get('remember.btn'),
                        baseCls: 'mButton',
                        cls: 'mButtonCentered',
                        pressedCls: 'mButtonPressed',
                        handler: this.onRememberTap,
                        scope: this
                    };

        this.add([
            topToolbar,
            labelError,
            fieldSet,
            rememberButton
        ]);
    },
    onBackTap: function () {
        this.fireEvent("backCommand", this);
    },
    config: {
        layout: {
            type: 'vbox'
        },
        scrollable: true
    }
});