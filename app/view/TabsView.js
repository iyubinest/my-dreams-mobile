Ext.define('MyDream.view.TabsView', {
    extend: 'Ext.tab.Panel',
    xtype: 'tabsview',
    alias: "widget.tabsview",
    myDreamsTab: null,
    createDreamTab: null,
    checkDreamTab: null,
    notificationTab: null,
    requires: [
        'Ext.TitleBar'
    ],
    onLogoutTap: function () {
        this.fireEvent("logoutCommand", this);
    },
    initialize: function () {

        this.callParent(arguments);
        
        var me = this;

        var logoutBtn = {
            xtype: "button",
            text: '',
            baseCls: 'mLogoutBtn',
            pressedCls: 'mLogoutBtnPressed',
            ui: 'back',
            align: 'right',
            itemId: 'backButton',
            handler: this.onLogoutTap,
            scope: this
        };

        this.myDreamsTab = {
            iconCls: 'mydream',
            items: [
                {
                    docked: 'top',
                    xtype: 'titlebar',
                    baseCls: 'mNavigationbar',
                    items:[
                        logoutBtn
                    ],
                    locales : {
                        title : 'tabs.dreamstab.title'
                    }
                },
                {
                    xtype: 'mydreams'
                }
            ]
        };

        this.createDreamTab = {
            iconCls: 'newdream',
            styleHtmlContent: true,
            scrollable: true,
            items: [
                {
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Crear sueño',
                    baseCls: 'mNavigationbar',
                    items:[
                        logoutBtn
                    ],
                    locales : {
                        title : 'tabs.newdreamtab.title'
                    }
                },
                {
                    xtype: 'createdream'
                }
            ]
        };

        this.checkDreamTab = {
            iconCls: 'checkdream',
            items: [
                {
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Verificación de Sueños',
                    baseCls: 'mNavigationbar',
                    items:[
                        logoutBtn
                    ],
                    locales : {
                        title : 'tabs.checkdreamtab.title'
                    }
                },
                {
                    xtype: 'checkeddreams'
                }
            ]
        };

        this.notificationTab = {
            iconCls: 'notify',
            items: [
                {
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Notificaciones',
                    baseCls: 'mNavigationbar',
                    items:[
                        logoutBtn
                    ],
                    locales : {
                        title : 'tabs.notificationtab.title'
                    }
                },
                {
                    xtype: 'notifications'
                }
            ]
        };

        this.add([
            me.myDreamsTab,
            me.createDreamTab,
            me.checkDreamTab,
            me.notificationTab
        ]);

        this.fireEvent("loadDataViewCommand", this);
    },
    config: {
        tabBarPosition: 'bottom',
        style: 'background-color: transparent',
        scrollable: true
    }
});
