Ext.define("MyDream.view.DreamStep", {
    extend: "Ext.Container",
    xtype: 'dreamstep',
    alias: "widget.dreamstep",
    requires: ['Ext.form.FieldSet', 'Ext.form.Password', 'Ext.Label', 'Ext.Img', 'Ext.util.DelayedTask','Ext.field.Select','Ext.field.DatePicker','Ext.field.Spinner','Ext.field.Toggle'],

    isCycle: false,
    priority: 3,
    stepindex: null,

    initialize: function () {
        this.callParent(arguments);
    },

    config: {
        layout: {
            type: 'vbox',
            fullscreen: true
        },
        baseCls: 'mCreateDream',
        scrollable: true
    },

    onDeleteStepButton: function(){
        var me = this;
        me.fireEvent("deleteStepCommand", this.stepindex);
    },
    
    onSaveStepButton: function(){
        var me = this;
        tituloTextField = me.down('#tituloTextField');
        dateTextField = me.down('#dateTextField');
        descTextField = me.down('#descTextField');
        periodTextField = me.down('#periodTextField');
        timeTextField = me.down('#timeTextField');
        label = me.down('#signInFailedLabel');

        titulo = tituloTextField.getValue();
        date = dateTextField.getValue();
        desc = descTextField.getValue();
        period = periodTextField.getValue();
        time = timeTextField.getValue();

        var valid = true;

        if(titulo === ''){
            valid = false;
            tituloTextField.addCls('errorInput');
        }
 
        // if(desc === ''){
        //     valid = false;
        //     descTextField.addCls('errorInput');
        // }

        var step = new Object();
        step.name = titulo;
        step.expiration = date;
        step.deadline = (Ext.Date.format(date, 'd-m-Y'));
        step.completed = false;
        step.description = desc;
        step.alert = true;
        step.weight = me.priority;
        if(this.isCycle){
            step.repeat = new Object();
            step.repeat.period = period;
            step.repeat.time = time;
        }

        if (valid){
            var task = Ext.create('Ext.util.DelayedTask', function () {
                label.setHtml('<p></p>');
                me.fireEvent("saveStepCommand", step,me.stepindex);
            });
            task.delay(500);
        }else{
            label.setHtml('<p>'+Ux.locale.Manager.get('form_validation_error')+'</p>');
        }

    },
    onBackTap: function(){
        this.fireEvent("backCommand", this);
    },
    onP1Tap: function(){
        var me = this;
        me.priority = 1;
        star1 = me.down('#pButton1');
        star2 = me.down('#pButton2');
        star3 = me.down('#pButton3');
        star4 = me.down('#pButton4');
        star5 = me.down('#pButton5');
        star1.setCls('mStarSelected');
        star2.removeCls('mStarSelected');
        star3.removeCls('mStarSelected');
        star4.removeCls('mStarSelected');
        star5.removeCls('mStarSelected');
    },
    onP2Tap: function(){
        var me = this;
        me.priority = 2;
        star1 = me.down('#pButton1');
        star2 = me.down('#pButton2');
        star3 = me.down('#pButton3');
        star4 = me.down('#pButton4');
        star5 = me.down('#pButton5');
        star1.setCls('mStarSelected');
        star2.setCls('mStarSelected');
        star3.removeCls('mStarSelected');
        star4.removeCls('mStarSelected');
        star5.removeCls('mStarSelected');
    },
    onP3Tap: function(){
        var me = this;
        me.priority = 3;
        star1 = me.down('#pButton1');
        star2 = me.down('#pButton2');
        star3 = me.down('#pButton3');
        star4 = me.down('#pButton4');
        star5 = me.down('#pButton5');
        star1.setCls('mStarSelected');
        star2.setCls('mStarSelected');
        star3.setCls('mStarSelected');
        star4.removeCls('mStarSelected');
        star5.removeCls('mStarSelected');
    },
    onP4Tap: function(){
        var me = this;
        me.priority = 4;
        star1 = me.down('#pButton1');
        star2 = me.down('#pButton2');
        star3 = me.down('#pButton3');
        star4 = me.down('#pButton4');
        star5 = me.down('#pButton5');
        star1.setCls('mStarSelected');
        star2.setCls('mStarSelected');
        star3.setCls('mStarSelected');
        star4.setCls('mStarSelected');
        star5.removeCls('mStarSelected');
    },
    onP5Tap: function(){
        var me = this;
        me.priority = 5;
        star1 = me.down('#pButton1');
        star2 = me.down('#pButton2');
        star3 = me.down('#pButton3');
        star4 = me.down('#pButton4');
        star5 = me.down('#pButton5');
        star1.setCls('mStarSelected');
        star2.setCls('mStarSelected');
        star3.setCls('mStarSelected');
        star4.setCls('mStarSelected');
        star5.setCls('mStarSelected');
    },
    hideCycleElements:function(){
        var me = this;
        this.isCycle = false;
        labelCycleCounterTextField = me.down('#labelCycleCounterTextField');
        labelCycleCounterTextField.hide();
        inputCycleCounterSpinnerField = me.down('#periodTextField');
        inputCycleCounterSpinnerField.hide();
        labelCycleSelectorTextField = me.down('#labelCycleSelectorTextField');
        labelCycleSelectorTextField.hide();
        inputCycleSelectorTextField = me.down('#timeTextField');
        inputCycleSelectorTextField.hide();
    },
    showCycleElements:function(){
        var me = this;
        this.isCycle = true;
        labelCycleCounterTextField = me.down('#labelCycleCounterTextField');
        labelCycleCounterTextField.show();
        inputCycleCounterSpinnerField = me.down('#periodTextField');
        inputCycleCounterSpinnerField.show();
        labelCycleSelectorTextField = me.down('#labelCycleSelectorTextField');
        labelCycleSelectorTextField.show();
        inputCycleSelectorTextField = me.down('#timeTextField');
        inputCycleSelectorTextField.show();
    },
    loadDataView: function(date,step,indexstep){

        var me = this;

        var backButton = {
            xtype: "button",
            text: '',
            baseCls: 'mBackBtn',
            pressedCls: 'mBackBtnPressed',
            ui: 'back',
            itemId: 'backButton',
            handler: this.onBackTap,
            scope: this
        };

        var topToolbar = {
            xtype: 'toolbar',
            docked: "top",
            baseCls: 'header',
            title: Ux.locale.Manager.get('dreamstep.add_step_btn'),
            items:[
                backButton
            ]
        };

        var labelError = {
                        xtype: 'label',
                        html: '<p>   </p>',
                        itemId: 'signInFailedLabel',
                        hidden: false,
                        baseCls: 'mLabel',
                        cls: 'error'
                    };

        var labelTitulo = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('dreamstep.name')
                    };

        var inputTitulo = {
                        xtype: 'textfield',
                        baseCls: 'mInputForm',
                        itemId: 'tituloTextField',
                        name: 'tituloTextField',
                        required: true
                    };

        var labelDate = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('dreamstep.date')
                    };

        var inputDate = {
            xtype: 'datepickerfield',
            value: new Date(),
            itemId: 'dateTextField',
            name: 'dateTextField',
            picker : {
                yearFrom: new Date().getFullYear(),
                yearTo: date.getFullYear(),
                enableLocale : true,
                locales: {
                    months : 'months'
                }
            }
        };

        var labelDesc = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('dreamstep.description')
                    };

        var inputDesc = {
                        xtype: 'textareafield',
                        baseCls: 'mInputForm',
                        itemId: 'descTextField',
                        name: 'descTextField',
                        required: true
                    };

        var labelCycle = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        itemId: 'labelCycleTextField',
                        name: 'labelCycleTextField',
                        html: Ux.locale.Manager.get('dreamstep.cycle')
                    };

        var inputCycle = {
                        xtype: 'togglefield',
                        baseCls: 'mInputForm',
                        itemId: 'inputCycleToggleField',
                        name: 'inputCycleToggleField',
                        required: true,
                        listeners:{
                            change: function(field, newValue, oldValue) {
                                if(newValue){
                                    me.showCycleElements();
                                }else{
                                    me.hideCycleElements();
                                }
                            }
                        }
                    };

        var labelCycleCounter = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('dreamstep.repeat'),
                        itemId: 'labelCycleCounterTextField',
                        name: 'labelCycleCounterTextField'
                    };

        var inputCycleCounter = {
                        xtype: 'spinnerfield',
                        baseCls: 'mInputForm',
                        itemId: 'periodTextField',
                        name: 'periodTextField',
                        required: true,
                        stepValue: 1,
                        minValue: 1,
                        maxValue: 7,
                        cycle: true
                    };

        var labelCycleSelector = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('dreamstep.by'),
                        itemId: 'labelCycleSelectorTextField',
                        name: 'labelCycleSelectorTextField'
                    };

        var inputCycleSelector = {
                    xtype: 'selectfield',
                    itemId: 'timeTextField',
                    name: 'timeTextField',
                    options: [
                        {text:Ux.locale.Manager.get('dreamstep.day'),value:'Day'},
                        {text:Ux.locale.Manager.get('dreamstep.week'),value:'Week'},
                        {text:Ux.locale.Manager.get('dreamstep.month'),value:'Month'}
                    ]
                };

        var labelPriority = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('dreamstep.priority')
                    };

        var pButton1 = {
            xtype: "button",
            text: '',
            baseCls: 'mStarNormal',
            itemId: 'pButton1',
            name: 'pButton1',
            handler: this.onP1Tap,
            scope: this
        };

        var pButton2 = {
            xtype: "button",
            text: '',
            baseCls: 'mStarNormal',
            itemId: 'pButton2',
            name: 'pButton2',
            handler: this.onP2Tap,
            scope: this
        };

        var pButton3 = {
            xtype: "button",
            text: '',
            baseCls: 'mStarNormal',
            itemId: 'pButton3',
            name: 'pButton3',
            handler: this.onP3Tap,
            scope: this
        };

        var pButton4 = {
            xtype: "button",
            text: '',
            baseCls: 'mStarNormal',
            itemId: 'pButton4',
            name: 'pButton4',
            handler: this.onP4Tap,
            scope: this
        };

        var pButton5 = {
            xtype: "button",
            text: '',
            baseCls: 'mStarNormal',
            itemId: 'pButton5',
            name: 'pButton5',
            handler: this.onP5Tap,
            scope: this
        };

        var starPanel = {
            xtype: 'container',
            layout: 'hbox',
            baseCls: 'mStarPanel',
            items: [
                pButton1,pButton2,pButton3,pButton4,pButton5
            ]
        }
        
        var fieldSet = {
                        xtype: 'fieldset',
                        items: [labelError,labelTitulo,inputTitulo,labelDate,inputDate,labelDesc,inputDesc,labelCycle,inputCycle,labelCycleCounter,inputCycleCounter,labelCycleSelector,inputCycleSelector,labelPriority,starPanel]
                    };

        var classModifierStepAlone = 'mButtonCentered';
        if(step){
            classModifierStepAlone = '';
        }

        var addStepButton = {
                        xtype: 'button',
                        itemId: 'addButton',
                        name: 'addButton',
                        ui: 'action',
                        padding: '10px',
                        text: Ux.locale.Manager.get('dreamstep.ok_btn'),
                        baseCls: 'mButton',
                        cls: classModifierStepAlone,
                        pressedCls: 'mButtonPressed',
                        handler: this.onSaveStepButton,
                        scope: this
                    };

        var deleteStepButton = {
                        xtype: 'button',
                        itemId: 'logInButton',
                        ui: 'action',
                        padding: '10px',
                        text: Ux.locale.Manager.get('dreamstep.delete_btn'),
                        baseCls: 'mButton',
                        pressedCls: 'mButtonPressed',
                        handler: this.onDeleteStepButton,
                        scope: this
                    };

        if(step){
            var buttonsWrapper = {
                xtype: 'panel',
                baseCls: 'margined',
                layout: {type:'hbox',align:'stretch'},
                items: [
                    addStepButton,deleteStepButton
                ]
            }
        }else{
            var buttonsWrapper = {
                xtype: 'panel',
                baseCls: 'margined',
                layout: {type:'hbox',align:'stretch'},
                items: [
                    addStepButton
                ]
            }
        }

        this.add([
            topToolbar,
            fieldSet,
            buttonsWrapper
        ]);

        if(step){
            this.add(deleteStepButton);
            this.stepindex = indexstep;


            var me = this;
            tituloTextField = me.down('#tituloTextField');
            dateTextField = me.down('#dateTextField');
            descTextField = me.down('#descTextField');
            periodTextField = me.down('#periodTextField');
            timeTextField = me.down('#timeTextField');
            label = me.down('#signInFailedLabel');

            addButton = me.down('#addButton');
            addButton.setText(Ux.locale.Manager.get('dreamstep.edit_btn'));

            tituloTextField.setValue(step.name);
            descTextField.setValue(step.description);
            dateTextField.setValue(step.expiration);
            if(step.weight==1){
                this.onP1Tap();
            }else if(step.weight==2){
                this.onP2Tap();
            }else if(step.weight==3){
                this.onP3Tap();
            }else if(step.weight==4){
                this.onP4Tap();
            }else if(step.weight==5){
                this.onP5Tap();
            }
            if(step.repeat){
                periodTextField.setValue(step.repeat.period);
                timeTextField.setValue(step.repeat.time);
                this.showCycleElements();
            }else{
                this.hideCycleElements();
            }
            
        }else{
            this.onP3Tap();
            this.hideCycleElements();
        }
        
    }
});