Ext.define("MyDream.view.CheckDream", {
    extend: "Ext.Container",
    xtype: 'checkdream',
    alias: "widget.checkdream",
    requires: ['Ext.form.FieldSet', 'Ext.form.Password', 'Ext.Label', 'Ext.Img', 'Ext.util.DelayedTask','Ext.field.Select','Ext.field.DatePicker','Ext.util.Geolocation'],

    mUrl: null,
    currentLat:null,
    currentLng:null,
    currentcity:'n/a',
    
    initialize: function () {
        this.callParent(arguments);
    },
    config: {
        layout: {
            type: 'vbox',
            fullscreen: true
        },
        baseCls: 'mCreateDream',
        scrollable: true
    },
    onSaveStepButton: function(){
        var me = this;
        descTextField = me.down('#descTextField');
        label = me.down('#signInFailedLabel');
        desc = descTextField.getValue();
        
        var valid = true;

        if(desc === ''){
            valid = false;
            descTextField.addCls('errorInput');
        }

        var check = new Object();
        check.desc = desc;
        check.city = me.currentcity;
        check.lat = me.currentLat;
        check.lng = me.currentLng;
        check.img = me.mUrl;
        
        if (valid){
            var task = Ext.create('Ext.util.DelayedTask', function () {
                label.setHtml('<p></p>');
                me.fireEvent("saveCheckCommand", check);
            });
            task.delay(500);
        }else{
            label.setHtml(Ux.locale.Manager.get('form_validation_error'));
        }

    },
    onBackTap: function(){
        this.fireEvent("backCommand", this);
    },
    loadDataView: function(item){
        var me = this;
        me.mUrl = item.getSrc();
        var image = item;

        var backButton = {
            xtype: "button",
            text: '',
            baseCls: 'mBackBtn',
            pressedCls: 'mBackBtnPressed',
            ui: 'back',
            itemId: 'backButton',
            handler: this.onBackTap,
            scope: this
        };

        var topToolbar = {
            xtype: 'toolbar',
            docked: "top",
            baseCls: 'header',
            title: Ux.locale.Manager.get('checkdream.title'),
            items:[
                backButton
            ]
        };

        var labelError = {
                        xtype: 'label',
                        html: '<p>   </p>',
                        itemId: 'signInFailedLabel',
                        hidden: false,
                        baseCls: 'mLabel',
                        cls: 'error'
                    };

        var dataPanel = {
            xtype: 'label',
            html: '<p>   </p>',
            itemId: 'positionLabel',
            hidden: false,
            baseCls: 'mLabel'
        };

        var geo = Ext.create('Ext.util.Geolocation', {
            autoUpdate: false,
            listeners: {
                locationupdate: function(geo) {
                    me.currentLat = geo.getLatitude();
                    me.currentLng =  geo.getLongitude();
                    var altitude = geo.getAltitude();
                    var speed = geo.getSpeed();
                    var heading= geo.getHeading();
                    Ext.Viewport.setMasked({
                        xtype: 'loadmask',
                        message: Ux.locale.Manager.get('please_wait')
                    });
                    Ext.Ajax.request({
                        url: 'http://maps.googleapis.com/maps/api/geocode/json',
                        method: 'get',
                        timeout: 60000,
                        params: {
                            latlng: me.currentLat+","+me.currentLng,
                            sensor: false
                        },
                        success: function (response) {
                            var loginResponse = Ext.JSON.decode(response.responseText);
                            for (var i = loginResponse.results[0].address_components.length - 1; i >= 0; i--) {
                                if(loginResponse.results[0].address_components[i].types[0]=="locality"){
                                    var locality = loginResponse.results[0].address_components[i].short_name;
                                    me.currentcity = locality;
                                    var label = me.down('#positionLabel');
                                    label.setHtml('<div class="mLocationCheck"><p>'+locality+'</p></div>');
                                }
                            };
                            Ext.Viewport.setMasked(false);
                        },
                        failure: function (response) {
                            Ext.Viewport.setMasked(false);
                        } 
                    });
                },
                locationerror: function(geo, bTimeout, bPermissionDenied, bLocationUnavailable, message) {
                    dataPanel.setHtml('<div class="dream-info clearfix"><h2>'+Ux.locale.Manager.get('checkdream.place_not_found')+'</h2></div>');
                }
            }
        });
        geo.updateLocation();
        

        var labelDesc = {
                        xtype: 'label',
                        baseCls: 'mLabel',
                        html: Ux.locale.Manager.get('checkdream.description')
                    };

        var inputDesc = {
                        xtype: 'textareafield',
                        baseCls: 'mInputForm',
                        cls: 'mLabel',
                        itemId: 'descTextField',
                        style: 'width: initial;',
                        name: 'descTextField',
                        required: true
                    };

        var fieldSet = {
                        xtype: 'fieldset',
                        items: [labelError,image,dataPanel,labelDesc,inputDesc]
                    };

        var addStepButton = {
                        xtype: 'button',
                        itemId: 'logInButton',
                        ui: 'action',
                        padding: '10px',
                        text: Ux.locale.Manager.get('checkdream.ok_button'),
                        baseCls: 'mButton',
                        cls: 'mButtonCentered',
                        pressedCls: 'mButtonPressed',
                        handler: this.onSaveStepButton,
                        scope: this
                    };

        this.add([
            topToolbar,
            fieldSet,
            addStepButton
        ]);

    }
});