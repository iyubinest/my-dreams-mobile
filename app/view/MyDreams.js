Ext.define('MyDream.view.MyDreams', {
    extend: 'Ext.Container',
    requires : ['Ext.dataview.List','MyDream.model.Notification'],
    xtype: 'mydreams',
    alias: "widget.mydreams",
    mStore: null,
    mList: null,

    updateData: function(){
        this.mStore.load();
        //this.mStore.sync();
    },
    initialize: function () {

        this.callParent(arguments);

        var me = this;

        me.mStore = new Ext.create('Ext.data.Store',{
                autoLoad: false, //NOTE: set to false so that I can manually call store.load()
                clearOnPageLoad: false,
                model: 'MyDream.model.Dream',
                fields: ['idDream','name', 'completion','completion_inverted', 'defaultImage','steps','deadline','type','imgs','description','value'],
                listeners:{
                    beforeload: function(store, opt){
                        store.data.clear();
                    },
                    load: function(store) {
                        Ext.Viewport.setMasked(false);
                    }
                },
                proxy: {
                    type: 'ajax',
                    timeout: 30000,
                    url: MyDream.app.BASE_URL+'mmydreams',
                    reader: {
                        type: 'json',
                        rootProperty: 'comp'
                    }
                }
            });

        me.mList = {
            xtype: 'list',
            itemTpl: new Ext.XTemplate('<div class="item-dream"><h3>{name}</h3><p class="percent-dream-completion"> {completion}% </p><div class="dream-color-overlay" style="height: {completion_inverted}% !important;"></div><img class="dream-cover" style="background-image:url({[this.linkify(values.defaultImage,values.idDream)]});background-size:contain;background-position:center;height:100%;width:100%;background-repeat:no-repeat;" /></div>',
            {
                linkify: function(response,id){
                    if(response.substring(0,10)=='data:image/'){
                        return response;
                    }
                    if(response==='' || response==undefined){
                        return MyDream.app.BASE_URL+'dream-pic?id='+id;
                    }
                    return MyDream.app.BASE_URL+response;
                }
            }),
            store: me.mStore,
            baseCls: 'mGridlist',
            plugins: [
                {
                    xclass: 'Ext.plugin.ListPaging', // part of Sencha Touch
                    autoPaging: false,                // page on swipe down
                    loadMoreText : Ux.locale.Manager.get('load_more'),  // text to show when more records
                    noMoreRecordsText : Ux.locale.Manager.get('load_more'),
                    pageSize: 10,
                    clearOnPageLoad: false
                },
                {
                    xclass: 'Ext.plugin.PullRefresh',
                    pullText: Ux.locale.Manager.get('slide_to_update'),
                    releaseText: Ux.locale.Manager.get('leave_to_update'),
                    loadedText: Ux.locale.Manager.get('updated'),
                    loadingText: Ux.locale.Manager.get('updating'),
                    lastUpdatedText: Ux.locale.Manager.get('last_update'),
                    baseCls: 'pull'                   
                }
            ],
            listeners:{
                itemtap: {fn: this.onItemTap, scope: this}
            }
        };
        this.add([me.mList])
        
        this.mStore.load();
    },
    config: {
        fullscreen: true,
        style: 'background-color: transparent;height:100%;width:100%;',
        layout: 'fit',
        navigationBar : {
            docked : 'top',
            items : [
                {
                    xtype: "button",
                    text: '',
                    baseCls: 'mLogoutBtn',
                    pressedCls: 'mBackBtnPressed',
                    ui: 'back',
                    itemId: 'backButton',
                    handler: this.onLogoutTap,
                    scope: this
                }
            ]
        }
    },
    onItemTap: function(list, index, target, record, e, eOpts){
        this.fireEvent("viewDreamCommand", record);
    }
});
