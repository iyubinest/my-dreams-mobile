Ext.define('MyDream.controller.Main', {
    extend: 'Ext.app.Controller',
    
    //configuracion de las referencias a las pantallas (refs = pantallas, control=funciones mapeadas)
    config: {
        refs: {
            loginView: 'login',
            recoveryView: 'remember',
            registerView: 'register',
            registerfbView: 'registerfb',
            tabsView: 'tabsview',
            myDreamsView: 'mydreams',
            dreamDetailView: 'dreamdetail',
            editDreamView: 'editdream',
            createDreamView: 'createdream',
            dreamStepView: 'dreamstep',
            dreamStepEditView: 'dreamstepedit',
            checkedDreamsView: 'checkeddreams',
            notificationsView: 'notifications',
            checkDreamView: 'checkdream',
            checkDetailView: 'checkdetail',
            selectDreamView: 'selectdream'
        },
        control: {
            loginView: {
                signInCommand: 'onSignInCommand',
                goRegisterCommand: 'onGoRegisterCommand',
                facebookCommand: 'onFacebookCommand',
                checkSignInCommand: 'onCheckSignInCommand',
                recoveryCommand: 'onRecoveryCommand'
            },
            recoveryView: {
                backCommand: 'onBackLoginCommand'
            },
            registerView: {
                registerCommand: 'onRegisterCommand',
                backCommand: 'onBackLoginCommand'
            },
            registerfbView: {
                registerCommand: 'onRegisterFBCommand',
                backCommand: 'onBackLoginCommand'
            },
            tabsView: {
                loadDataViewCommand: 'onLoadDataViewCommand',
                logoutCommand: 'onLogoutCommand'
            },
            myDreamsView: {
                reloadDreamsCommand: 'onReloadDreamsCommand',
                viewDreamCommand: 'onViewDreamCommand'
            },
            dreamDetailView: {
                reloadDreamCommand: 'onReloadDreamCommand',
                backCommand: 'onBackCommand',
                editDreamCommand: 'onEditDreamCommand'
            },
            editDreamView: {
                saveDreamCommand: 'onSaveEditDreamCommand',
                backCommand: 'onBackEditCommand',
                createStepCommand: 'onCreateStepEditCommand'
            },
            createDreamView: {
                takePictureCommand: 'onTakePictureCommand',
                saveDreamCommand: 'onSaveDreamCommand',
                createStepCommand: 'onCreateStepCommand'
            },
            dreamStepView: {
                saveStepCommand: 'onSaveStepCommand',
                deleteStepCommand: 'onDeleteStepCommand',
                backCommand: 'onBackCommand'
            },
            dreamStepEditView: {
                saveStepCommand: 'onSaveStepEditCommand',
                deleteStepCommand: 'onDeleteStepEditCommand',
                backCommand: 'onBackStepEditCommand'
            },
            checkedDreamsView: {
                checkDreamCommand: 'onCheckDreamCommand',
                viewCheckedDreamCommand: 'onViewCheckedDreamCommand',
                viewCheckCommand: 'viewCheckCommand'
            },
            notificationsView: {
                reloadNotificationsCommand: 'onReloadNotificationsCommand'
            },
            checkDreamView: {
                saveCheckCommand: 'onSaveCheckCommand',
                backCommand: 'onBackCommand'
            },
            checkDetailView:{
                backCommand: 'onBackCommand'
            },
            selectDreamView:{
                backCommand: 'onBackCommand'
            }
        }
    },
    
    //Se llama cuando se inicia el controlador, se usa para cargar el evneto de inicio por facebook
    launch: function(app) {
        try{
            FB.Event.subscribe('auth.login', this.onResume);
        }catch(err){}
    },

    sessionToken: null,
    username: null,
    password: null,
    deviceId: '',

    // Transitions (Animaciones para la navegación)
    getSlideLeftTransition: function () {
        return { type: 'slide', direction: 'left' };
    },

    getSlideRightTransition: function () {
        return { type: 'slide', direction: 'right' };
    },

    //Se definen los metodos de la navegacion para todos las pantallas
    //todas las variables llamadas 'me' hacen referencia al controlador para poder acceder funcionalmente siempre
    //desde una funcion

    //LoginView
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //funcion para hacer el inicio de sesion
    onSignInCommand: function (view, username, password) {
        //referencia al controlador
        var me = this;
        //referencia a la vista login
        var loginView = this.getLoginView();
        //chequear si los parametros tienen contenido
        if ( (username+"").length === 0 || (password+"").length === 0) {
            //si no tenemos contenido en las variables mostramos un mensaje de error
            loginView.showSignInFailedMessage(Ux.locale.Manager.get('controller.login_msg_1'));
            return;
        }

        //obtenemos la referencia al token push del localstorage
        me.deviceId = localStorage.getItem("pushToken");

        //enmascaramos el viewport de la app
        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: Ux.locale.Manager.get('please_wait')
        });

        //se ejecuta la peticion para el inicio de sesion
        Ext.Ajax.request({
            url: MyDream.app.BASE_URL+'mlogin',
            method: 'post',
            timeout: 10000,
            params: {
                username: username,
                password: password,
                device: me.deviceId
            },
            success: function (response) {
                //se verifica que la peticion sea satisfactoria
                var loginResponse = Ext.JSON.decode(response.responseText);
                var loginValidation = loginResponse.status + "";
                me.sessionToken = loginResponse.comp + "";
                me.username = username;
                me.password = password;
                if (loginValidation === "true") {
                    //se guarda la informacion de login para poder hacer el acceso
                    localStorage.setItem("username",username);
                    localStorage.setItem("password",password);
                    localStorage.setItem("key",loginResponse.key);
                    localStorage.setItem("isFB",'false');
                    //cambiamos a la pantalla de inicio
                    me.signInSuccess();
                } else {
                    //se muestra mensaje de error de inicio de sesion
                    me.signInFailure(Ux.locale.Manager.get('controller.login_msg_2'));
                    //quitamos la mascara
                    Ext.Viewport.setMasked(false);
                }
            },
            failure: function (response) {
                //se muestra mensaje de error de inicio de sesion
                me.signInFailure(Ux.locale.Manager.get('controller.login_msg_2'));
                //quitamos la mascara
                Ext.Viewport.setMasked(false);
            } 
        });
    },

    //funcion para hacer el chequeo de la sesion
    onCheckSignInCommand: function(){
        //referencia al controlador
        var me = this;

        //obtenemos una 
        username = localStorage.getItem("username");
        password = localStorage.getItem("password");
        isFB = localStorage.getItem("isFB");
        key = localStorage.getItem("key");
        this.sessionToken=key;

        if(isFB=='true'){
            try{
                FB.login(null,{ scope: "email" });
            }catch(err){}
        }else{
            if(key!=null && key!=undefined){
                me.deviceId = localStorage.getItem("pushToken");
                Ext.Viewport.setMasked({
                    xtype: 'loadmask',
                    message: Ux.locale.Manager.get('please_wait')
                });
                Ext.Ajax.request({
                    url: MyDream.app.BASE_URL+'mlogin',
                    method: 'post',
                    timeout: 10000,
                    params: {
                        username: username,
                        password: password,
                        device: me.deviceId
                    },
                    success: function (response) {
                        try{
                            var loginResponse = Ext.JSON.decode(response.responseText);
                            var loginValidation = loginResponse.status + "";
                            me.sessionToken = loginResponse.comp + "";
                            me.username = username;
                            me.password = password;
                            if (loginValidation === "true") {
                                var tabsView = Ext.Viewport.add(Ext.create('MyDream.view.TabsView'));
                                Ext.Viewport.animateActiveItem(tabsView, me.getSlideLeftTransition());
                            }else{
                                Ext.Viewport.setMasked(false);
                            }
                        }catch(err){
                            Ext.Viewport.setMasked(false);
                        }
                    },
                    failure: function (response) {
                        Ext.Viewport.setMasked(false);
                    } 
                });
            }
        }
    },

    signInSuccess: function () {
        var loginView = this.getLoginView();
        //Ext.Viewport.setMasked(false);
        var tabsView = Ext.Viewport.add(Ext.create('MyDream.view.TabsView'));
        Ext.Viewport.animateActiveItem(tabsView, this.getSlideLeftTransition());
    },

    signInFailure: function (message) {
        var loginView = this.getLoginView();
        loginView.showSignInFailedMessage(message);
        Ext.Viewport.setMasked(false);
    },

    onGoRegisterCommand: function () {
        var registerView = Ext.Viewport.add(Ext.create('MyDream.view.Register'));
        Ext.Viewport.animateActiveItem(registerView, this.getSlideLeftTransition());
    },

    onFacebookCommand: function () {
        try{
            FB.login(null,{ scope: "email" });
        }catch(err){
            this.onResume();
        }
        // var registerfbView = Ext.Viewport.add(Ext.create('MyDream.view.RegisterFB'));
        // registerfbView.loadDataView("hola","hola2","hola3",'1111');
        // Ext.Viewport.animateActiveItem(registerfbView, this.getSlideLeftTransition());
    },

    facebookResponseCheck: function(userId,accessToken){
        var me = this;
        me.deviceId = localStorage.getItem("pushToken");
        Ext.Ajax.request({
            url: MyDream.app.BASE_URL+'mfblogin',
            method: 'post',
            timeout: 10000,
            params: {
                user: userId,
                token: accessToken,
                device: me.deviceId
            },
            success: function (response) {
                var loginResponse = Ext.JSON.decode(response.responseText);
                var loginValidation = loginResponse.status + "";
                if(loginValidation === 'true'){
                    localStorage.setItem("username",userId);
                    localStorage.setItem("password",accessToken);
                    localStorage.setItem("isFB",'true');
                }else{
                    me.signInFailure(Ux.locale.Manager.get('controller.login_msg_2'));
                    Ext.Viewport.setMasked(false);
                    return;
                }
                var loginValidationRegister = loginResponse.comp.action + "";
                me.username = userId;
                me.password = accessToken;
                
                if (loginValidation === "true" && loginValidationRegister === "main") {
                    me.signInSuccess();
                } else if (loginValidation === "true" && loginValidationRegister === "register"){
                    var fbusername = userId;
                    var fbemail = loginResponse.comp.user.email;
                    var fbname = loginResponse.comp.user.name;
                    var registerfbView = Ext.Viewport.add(Ext.create('MyDream.view.RegisterFB'));
                    registerfbView.loadDataView(fbusername,fbemail,fbname,accessToken);
                    Ext.Viewport.animateActiveItem(registerfbView, me.getSlideLeftTransition());
                } else {
                    me.signInFailure(Ux.locale.Manager.get('controller.login_msg_2'));
                }
                Ext.Viewport.setMasked(false);
            },
            failure: function (response) {
                me.signInFailure(Ux.locale.Manager.get('controller.login_msg_2'));
                Ext.Viewport.setMasked(false);
            }
        });
    },

    onResume: function(){
        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: Ux.locale.Manager.get('please_wait')
        });
        if(MyDream.app.isDevice){
            try{
                FB.getLoginStatus(function(session) {
                    if(session.authResponse!=null && session.authResponse!=undefined){
                        MyDream.app.getController('Main').facebookResponseCheck(session.authResponse.userId,session.authResponse.accessToken);
                    }else{
                        Ext.Viewport.setMasked(false);
                    }
                });
            }catch(err){
                Ext.Viewport.setMasked(false);
            }
        }else{
            MyDream.app.getController('Main').facebookResponseCheck(MyDream.app.fbTestUserID,MyDream.app.fbTestUserToken);    
        }        
    },

    onRecoveryCommand: function(){
        var rememberView = Ext.Viewport.add(Ext.create('MyDream.view.Remember'));
        Ext.Viewport.animateActiveItem(rememberView, this.getSlideLeftTransition());
    },

    //RegisterView
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    onRegisterCommand: function (view,username,password) {
        this.onSignInCommand(view,username,password);
    },

    onRegisterFBCommand:function(view,username,password){
        this.facebookResponseCheck(username,password);    
    },

    onBackLoginCommand: function () {
        Ext.Viewport.remove(Ext.Viewport.getActiveItem(), true);  
        var loginView = this.getLoginView();
        Ext.Viewport.animateActiveItem(loginView, this.getSlideRightTransition());
    },
    
    onBackCommand: function (something,update) { 
        var lastItem = Ext.Viewport.getActiveItem();
        var tabsView = this.getTabsView();
        var myDreamsView = this.getMyDreamsView();
        var checkedDreamsView = this.getCheckedDreamsView();
        var notificationsView = this.getNotificationsView();
        if(update==true){
            myDreamsView.updateData();
            checkedDreamsView.updateData();
            notificationsView.updateData();
        }
        Ext.Viewport.animateActiveItem(tabsView, this.getSlideRightTransition());

        var task = Ext.create('Ext.util.DelayedTask', function () {
            Ext.Viewport.remove(lastItem, true); 
        });
        task.delay(500);
        
    },

    //TabsView
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    onLoadDataViewCommand: function () {
        myDreamsView = this.getMyDreamsView();
        myCheckedDreamsView = this.getCheckedDreamsView();
        myNotificationsDreamsView = this.getNotificationsView();
        //myDreamsView.loadDataView(this.sessionToken);
        //myCheckedDreamsView.loadDataView(this.sessionToken);
    },

    onLogoutCommand: function(){
        var me = this;
        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: Ux.locale.Manager.get('please_wait')
        });
        Ext.Ajax.request({
            url: MyDream.app.BASE_URL+'mlogout',
            method: 'post',
            success: function (response) {
                var loginResponse = Ext.JSON.decode(response.responseText);
                var loginValidation = loginResponse.status + "";
                if (loginValidation === "true") {
                    window.localStorage.clear();
                    var loginView = me.getLoginView();
                    Ext.Viewport.animateActiveItem(loginView, me.getSlideRightTransition());
                    var cookies = document.cookie.split(";");

                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = cookies[i];
                        var eqPos = cookie.indexOf("=");
                        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
                        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
                    }
                }
                Ext.Viewport.setMasked(false);
            },
            failure: function (response) {
                Ext.Viewport.setMasked(false);
            } 
        });
    },

    //MyDreamsView
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    onReloadDreamsCommand: function () {
        console.log("init function");
    },

    onViewDreamCommand: function (record) {
        var dreamDetail = Ext.Viewport.add(Ext.create('MyDream.view.DreamDetail'));
        var me = this;
        var me_record = record;
        dreamDetail.loadDataView(me_record);
        Ext.Viewport.animateActiveItem(dreamDetail, me.getSlideLeftTransition());
    },

    viewCheckCommand: function(record){
        var checkDetail = Ext.Viewport.add(Ext.create('MyDream.view.CheckDetail'));
        var me = this;
        var me_record = record;
        checkDetail.loadDataView(me_record);
        Ext.Viewport.animateActiveItem(checkDetail, me.getSlideLeftTransition());
    },

    //DreamDetailView
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    onReloadDreamCommand: function () {
        
    },

    onAddStepCommand: function () {
        
    },

    onEditDreamCommand: function (response) {
        var editDreamView = Ext.Viewport.add(Ext.create('MyDream.view.EditDream'));
        editDreamView.loadDataView(response);
        Ext.Viewport.animateActiveItem(editDreamView, this.getSlideLeftTransition());
    },

    //CreateDreamView
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    onTakePictureCommand: function () {
        console.log("init function take picture");
    },

    onSaveDreamCommand: function () {
        var tabsView = this.getTabsView();
        tabsView.setActiveItem(0);
        var myDreamsView = this.getMyDreamsView();
        myDreamsView.updateData();
    },

    onCreateStepCommand: function (date,step,id) {
        var stepView = Ext.Viewport.add(Ext.create('MyDream.view.DreamStep'));
        stepView.loadDataView(date,step,id);
        Ext.Viewport.animateActiveItem(stepView, this.getSlideLeftTransition());
    },

    //EditDreamView
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    onSaveEditDreamCommand: function () {
        var tabsView = this.getTabsView();
        tabsView.setActiveItem(0);
        var myDreamsView = this.getMyDreamsView();
        myDreamsView.updateData();
        Ext.Viewport.animateActiveItem(tabsView, this.getSlideRightTransition());
    },

    onSaveEditStepDreamCommand: function () {
        
    },

    onCreateStepEditCommand: function (date,step,id) {
        var stepView = Ext.Viewport.add(Ext.create('MyDream.view.DreamStepEdit'));
        stepView.loadDataView(date,step,id);
        Ext.Viewport.animateActiveItem(stepView, this.getSlideLeftTransition());
    },

    onBackEditCommand: function () {
        Ext.Viewport.remove(Ext.Viewport.getActiveItem(), true);  
        var detailView = this.getDreamDetailView();
        Ext.Viewport.animateActiveItem(detailView, this.getSlideRightTransition());
    },
    
    //checkedDreamsView
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    onCheckDreamCommand: function (item) {
        var checkDreamView = Ext.Viewport.add(Ext.create('MyDream.view.CheckDream'));
        checkDreamView.loadDataView(item);
        Ext.Viewport.animateActiveItem(checkDreamView, this.getSlideLeftTransition());
    },

    onViewCheckedDreamCommand: function () {
        console.log("init function");
    },
    
    //notificationsView
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    onReloadNotificationsCommand: function () {
        console.log("init function");
    },
    
    //dreamStepView
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    onSaveStepCommand: function (step,stepindex) {
        var createView = this.getCreateDreamView();
        createView.addStep(step,stepindex);
        this.onBackCommand();
    },

    onDeleteStepCommand: function (stepindex) {
        var createView = this.getCreateDreamView();
        createView.deleteStep(stepindex);
        this.onBackCommand();
    },
    
    //dreamStepView
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    onSaveStepEditCommand: function (step,stepindex) {
        var editView = this.getEditDreamView();
        editView.addStep(step,stepindex);
        this.onBackStepEditCommand();
    },

    onDeleteStepEditCommand: function (stepindex) {
        var editView = this.getEditDreamView();
        editView.deleteStep(stepindex);
        this.onBackStepEditCommand();
    },

    onBackStepEditCommand: function () {
        Ext.Viewport.remove(Ext.Viewport.getActiveItem(), true);  
        var editView = this.getEditDreamView();
        Ext.Viewport.animateActiveItem(editView, this.getSlideRightTransition());
    },

    //checkDreamView
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    onSaveCheckCommand: function(check){
        var dreamSelectView = Ext.Viewport.add(Ext.create('MyDream.view.SelectDream'));
        dreamSelectView.loadDataView(check);
        Ext.Viewport.animateActiveItem(dreamSelectView, this.getSlideLeftTransition());
    }
    
});
