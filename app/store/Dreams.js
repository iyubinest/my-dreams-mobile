Ext.define('MyDream.store.Dreams', {
    extend: 'Ext.data.Store',
    xtype: 'Dreams',
    requires: [
        'MyDream.model.Dream'
    ],
    config: {
        model: 'MyDream.model.Dream',
        autoLoad: false, //NOTE: set to false so that I can manually call store.load()
        clearOnPageLoad: false,
        pageSize: 10,
        fields: ['id','dreamname', 'dreampercent', 'dreamimage', 'dreampercent_cover'],
        proxy: {
            type: 'ajax',
            url: 'http://www.colombiagames.com/presidencia/public/index/testdreams',
            reader: {
                type: 'json',
                rootProperty: 'data'
            }
        }
    }
});