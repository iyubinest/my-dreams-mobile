Ext.define('MyDream.store.Notifications', {
    extend: 'Ext.data.Store',
    xtype: 'Notifications',
    requires: [
        'MyDream.model.Notification'
    ],
    config: {
        model: 'MyDream.model.Notification',
        autoLoad: false,
        clearOnPageLoad: false,
        pageSize: 10,
        fields: ['titulo', 'thumbnail', 'id_articulo'],
        proxy: {
            type: 'ajax',
            url: 'http://apppresidencia.colombiagames.com/services/getarticleswithsubcategoryid?id=3',
            reader: {
                type: 'json',
                rootProperty: 'data'
            }
        }
    }
});