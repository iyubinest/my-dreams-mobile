Ext.define('MyDream.model.CheckDream', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: 'id',
        fields: ['id','name','date', 'dream', 'lat', 'lat', 'city', 'img']
    }
});