Ext.define('MyDream.model.Notification', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: 'id',
        fields: ['id','subject', 'description', 'date']
    }
});