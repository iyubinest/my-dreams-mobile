Ext.define('MyDream.model.Dream', {
    extend: 'Ext.data.Model',
    config: {
        idProperty: 'idDream',
        fields: ['idDream','name', 'completion','completion_inverted', 'defaultImage','steps','deadline','type','imgs','description','value']
    }
});